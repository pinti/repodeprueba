#!/usr/bin/python3
def factorial(valor):
    for i in range(1, valor):
        valor *= i
    return valor


if __name__ == "__main__":
    for n in range(1, 11):
        resultado = factorial(n)
        print(f"El factorial de {n} es {resultado}")
